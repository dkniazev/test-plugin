package test_plugin.ui

import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.DefaultActionGroup
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.SimpleToolWindowPanel
import com.intellij.util.ui.JBUI
import test_plugin.actions.AddElementAction
import javax.swing.JPanel

/**
 * @author Dmitrii_Kniazev
 * @since 02/20/2018
 */
class TestToolWindow(project: Project) : SimpleToolWindowPanel(true, true) {
    private val myProject: Project = project

    init {
        setToolbar(createToolbarPanel())
    }

    private fun createToolbarPanel(): JPanel {
        val group = DefaultActionGroup()
        group.add(AddElementAction())
        val actionToolbar = ActionManager.getInstance().createActionToolbar("TestToolbar", group, true)
        return JBUI.Panels.simplePanel(actionToolbar.component)
    }
}