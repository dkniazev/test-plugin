package test_plugin

import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import test_plugin.ui.TestToolWindow

class CassandraToolWindowFactory : ToolWindowFactory {
    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val testToolWindow = TestToolWindow(project)
        val contentManager = toolWindow.contentManager
        val content = contentManager.factory.createContent(testToolWindow, null, false)
        contentManager.addContent(content)
    }
}
